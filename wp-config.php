<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'serenDB');

/** MySQL database username */
define('DB_USER', 'serenDB');

/** MySQL database password */
define('DB_PASSWORD', ']9.PS86izD');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'buesflgqcsipsumglbzgwkkkor6ddf2lzsg7f8tsbkydyqnklodxecvsm9e4mrzq');
define('SECURE_AUTH_KEY',  'na9ygvoonjnncyu0ucsom9wbv9f0tfbevnjvgi7lue2sen0czvtmfxgfk3kwlvls');
define('LOGGED_IN_KEY',    'wncboew71gxyp3o8mqyf55tlaclbryxasvfczekqlmsfjasosqurokcedzh4jmel');
define('NONCE_KEY',        'rnjvimgynmwfegt0ro0p162f2o3dfxtqrtistpazjneivrlhyawe5zosr8dilifo');
define('AUTH_SALT',        '609jhi6jd90s3iabe2wgubcmfbjeubgwi63xipwt8yy0be0e5nn9qsxwyqykwdz9');
define('SECURE_AUTH_SALT', '0sryxg7g4ra0kghjvjtwwsmzvn0g3nnrmli74voey1y4xhg7acnusevezxssbh7y');
define('LOGGED_IN_SALT',   'lvxfbnldvyssqpzjwpwyppdx1eiqhckqd3ueb106e0x9yflnamk8k1bwheqvx0va');
define('NONCE_SALT',       'qo8mq3pk7hoi1do2fxx75d26xfwmfysrqsf5f1vb7ksgbur88wl6syrmckxuzpec');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
